package com.tsystems.javaschool.tasks.pyramid;

import javax.management.ObjectName;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

        if (inputNumbers.contains(null)){
            throw new CannotBuildPyramidException();
        }
        try {
            inputNumbers.sort((o1, o2) -> o1 - o2);
        }
        catch (OutOfMemoryError e) {
            throw new CannotBuildPyramidException();
        }


        int cnt=0;
        int row=0;
        while(cnt<inputNumbers.size()&&cnt>-1){
            row++;
            cnt+=row;
        }

        if (cnt>inputNumbers.size()||cnt<0) {
           throw new CannotBuildPyramidException();
        }
        int cols=row*2-1;
        int center=(int)Math.floor(cols/2);
        int [][] arr2D = new int[row][cols];

        row=0;
        cnt=row+1;

        int initCol=center;
        int col=initCol;
        for (int elem:inputNumbers){
            if (cnt==0){
                row++;
                cnt=row+1;
                initCol--;
                col=initCol;
            }
            arr2D[row][col]=elem;
            cnt--;
            col+=2;
        }

        return arr2D;
    }


}
